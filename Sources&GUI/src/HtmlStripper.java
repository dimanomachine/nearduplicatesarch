/*
 * Created on May 12, 2005
 */

/**
 * @author object
 */
public class HtmlStripper {
	public static String strip(final String strHtmlString)
	{
		int nLength = strHtmlString.length();
		char[] arrResult = new char[nLength];
		int j = 0;
		for (int i = 0; i < nLength; i++) {
			final char c = strHtmlString.charAt(i);
			if ('<' == c) {
				if (i + 1 < nLength) {
					char d = strHtmlString.charAt(i + 1);
					if (canBeFirstInTag(d)) {
						i = strHtmlString.indexOf('>', i);
						if (i < 0) {
							break;
						} else {
							continue;
						}
					}
				}
			}
			if (accept(c)) {
				arrResult[j++] = c;				
			}
		}
		return String.copyValueOf(arrResult, 0, j).toLowerCase();
	}

	private static boolean accept(char c) {
		return ' ' != c && '\r' != c && '\n' != c;
	}

	private static boolean canBeFirstInTag(char c) {
		return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z') || ('/' == c) || ('!' == c);
	}
}
