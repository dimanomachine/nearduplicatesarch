
import util.StringUtil;
import util.comparators.ComparatorUtil;
import util.collection.IndexedHashMap;

import java.util.*;
import java.util.zip.DataFormatException;
import java.io.*;

/*
����������� ������ ������� ������ ����
1) ����� ������������ ���;
2) ����� ����� ���;
3) ����� ���, ������� ���� � � ���, � � �������;
4) ������ ���, ������� ���� ������ � �������;
5) ������ ���, ������� ���� ������ � ���.

� ������ ����� ����������� ���������� ��������� ������ ���� �����������,
������� �� ����������. �.�., ���� � ����� ��������� ���������� �����������
�����-�� ��������, �� ��������� ������������ ���� � ��� ��������.
*/

public class PagesDblComparator
{
    private static final String DESCRIPTION_DELIMITER = " ";
    private static final int HASHMAP_INITIAL_CAPACITY = 1000;

    private HashSet m_yandexDblPagesSet = new HashSet();
    private IndexedHashMap m_vinitiDblPagesMap = new IndexedHashMap(HASHMAP_INITIAL_CAPACITY);
    private IndexToIndexMap m_indicesMap = null;

    private PrintWriter m_pwFileIds;

    private static class HashedIntPair
    {
        int m_pairHash;
        Integer m_firstItem;
        Integer m_secondItem;

        HashedIntPair(final Integer firstItem, final Integer secondItem)
        {
            m_firstItem = firstItem;
            m_secondItem = secondItem;
            m_pairHash = 31*firstItem.hashCode() + secondItem.hashCode();
        }

        public int hashCode()
        {
            return m_pairHash;
        }

        public boolean equals(Object o)
        {
            if(this == o) return true;
            if(!(o instanceof HashedIntPair)) return false;

            final HashedIntPair hashedIntPair = (HashedIntPair) o;

            if(m_pairHash != hashedIntPair.m_pairHash) return false;

            return true;
        }

        public Integer getFirstItem()
        {
            return m_firstItem;
        }

        public Integer getSecondItem()
        {
            return m_secondItem;
        }

        public String toString()
        {
            return "\n" + m_firstItem + DESCRIPTION_DELIMITER + m_secondItem;
        }
    }

    PagesDblComparator()
    {
    }

    private void makeStatistics(String[] args) throws IOException, DataFormatException
    {
        m_pwFileIds = new PrintWriter(new FileWriter(args[0]), true);
        String yandexDblPagesUrl = args[1];
        String yandexRepositoryUrl = args[2];
        String vinitiDblPagesUrl = args[3];
        String vinitiRepositoryUrl = args[4];

        FileIdsComparator fileIdsComparator = new FileIdsComparator();
        m_indicesMap = fileIdsComparator.mapIndices(yandexRepositoryUrl, vinitiRepositoryUrl);

        load(yandexDblPagesUrl, true);
        load(vinitiDblPagesUrl, false);

        compareFiles();
    }

    private void compareFiles()
    {
        HashSet yandexPairs = m_yandexDblPagesSet;
        HashSet vinitiPairs = getSetOfVinitiDblPairs();

        int commonPairsNumber = 0;

        Object[] vinitiPairsArray = vinitiPairs.toArray();
        for(int i = 0; i < vinitiPairsArray.length; i++)
        {
            final HashedIntPair commonObject = (HashedIntPair)vinitiPairsArray[i];
            if(yandexPairs.contains(commonObject))
            {
                commonPairsNumber++;
                yandexPairs.remove(commonObject);
                vinitiPairs.remove(commonObject);
            }
        }

        LinkedList yandexPairsList = sortDblPairsSet(yandexPairs);
        LinkedList vinitiPairsList = sortDblPairsSet(vinitiPairs);

        m_pwFileIds.println("yandex dbl pairs: " + (m_yandexDblPagesSet.size()));
        m_pwFileIds.println("viniti dbl pairs: " + (m_vinitiDblPagesMap.size() / 2));
        m_pwFileIds.println("common pairs: " + commonPairsNumber);
        m_pwFileIds.println("viniti only pairs: \n" + vinitiPairsList.toString());
        m_pwFileIds.println("yandex only pairs: \n" + yandexPairsList.toString());
    }

    private LinkedList sortDblPairsSet(HashSet pairsHashSet)
    {
        Object[] pairsArray = pairsHashSet.toArray();

        Arrays.sort(pairsArray, new Comparator()
        {
            public int compare(Object o1, Object o2)
            {
                final HashedIntPair hashedIntPair1 = (HashedIntPair) o1;
                final HashedIntPair hashedIntPair2 = (HashedIntPair) o2;
                return ComparatorUtil.compareInt(hashedIntPair1.hashCode(), hashedIntPair2.hashCode());
            }
        });

        LinkedList pairsList = new LinkedList();

        for(int i = 0; i < pairsArray.length; i++)
        {
            pairsList.add(pairsArray[i]);
        }

        return pairsList;
    }

    private HashSet getSetOfVinitiDblPairs()
    {
        HashSet pairsSet = new HashSet();

        for(int i = 0; i < m_vinitiDblPagesMap.size(); i++)
        {
            final Integer key = (Integer)m_vinitiDblPagesMap.getKey(i);
            final Object[] values = ((HashSet)m_vinitiDblPagesMap.get(key)).toArray();

            for(int j = 0; j < values.length; j++)
            {
                final Integer secondItem = (Integer)values[j];
                pairsSet.add(new HashedIntPair(key, secondItem));

                HashSet hashSetForSecondItem = (HashSet)m_vinitiDblPagesMap.get(secondItem);
                if(null != hashSetForSecondItem)
                {
                    hashSetForSecondItem.remove(key);
                }
            }
        }

        return pairsSet;
    }

    private void load(String url, final boolean isYandexFormat) throws IOException, DataFormatException
    {
        InputStreamReader inputStreamReader = null;
        FileInputStream inputStreamBytes = null;

        try
        {
            inputStreamBytes = new FileInputStream(url);
            inputStreamReader = new InputStreamReader(inputStreamBytes);
            if(isYandexFormat)
            {
                readYandexFormat(inputStreamReader);
            }
            else
            {
                readVinitiFormat(inputStreamReader);
            }
        }
        finally
        {
            if(null != inputStreamBytes)
            {
                inputStreamBytes.close();
                inputStreamBytes = null;
            }

            if(null != inputStreamReader)
            {
                inputStreamReader.close();
                inputStreamReader = null;
            }
        }
    }

    private void readYandexFormat(InputStreamReader r) throws IOException, DataFormatException
    {
        LineNumberReader reader = new LineNumberReader(r);
        for(String strLine = reader.readLine(); strLine != null; strLine = reader.readLine())
        {
            strLine = strLine.trim();
            if(shouldParseLine(strLine))
            {
                final StringTokenizer oTokenizer = new StringTokenizer(strLine, DESCRIPTION_DELIMITER);

                if(3 != oTokenizer.countTokens())
                {
                    throw new DataFormatException("Illegal file description: " + strLine + " (tokens: " + oTokenizer.countTokens() + ")");
                }

                String strFirstId = oTokenizer.nextToken();
                String strSecondId = oTokenizer.nextToken();
                final Integer firstId = Integer.decode(strFirstId);
                final Integer secondId = Integer.decode(strSecondId);

                if(m_indicesMap.checkYandexId(firstId) && m_indicesMap.checkYandexId(secondId))
                {
                    m_yandexDblPagesSet.add(new HashedIntPair(firstId, secondId));
                }
            }
        }

        if(null != reader)
        {
            reader.close();
        }
    }

    private void readVinitiFormat(InputStreamReader r) throws IOException
    {
        LineNumberReader reader = null;

        try
        {
            reader = new LineNumberReader(r);
            for(String strLine = reader.readLine(); strLine != null; strLine = reader.readLine())
            {
                strLine = strLine.trim();
                if(shouldParseLine(strLine))
                {
                    StringTokenizer oTokenizer = new StringTokenizer(strLine, DESCRIPTION_DELIMITER);
                    final int numberOfTokens = oTokenizer.countTokens();

                    if(numberOfTokens < 3)
                    {
                        continue;
                    }

                    final LinkedList list = new LinkedList();

                    for(int i = 0; i < numberOfTokens - 1; i++)
                    {
                        String key = oTokenizer.nextToken().trim();
                        final Integer yandexId = m_indicesMap.getViniti2YandexIds(Integer.decode(key));
                        if(null != yandexId)
                        {
                            list.add(yandexId);
                        }
                    }

                    if(1 == list.size())
                    {
                        continue;
                    }

                    for(int i = 0; i < list.size(); i++)
                    {
                        if(0 < i)
                        {
                            swap(list, 0, i);
                        }

                        final HashSet hashSet = new HashSet();
                        for(int j = 1; j < list.size(); j++)
                        {
                            hashSet.add(list.get(j));
                        }
                        putIndicesToSet(m_vinitiDblPagesMap, (Integer)list.get(0), hashSet);
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        finally
        {

            if(null != reader)
            {
                reader.close();
            }
        }
    }

    private static void swap(LinkedList order, int index, int j)
    {
        assert (index > 0) && (index < order.size()) && (j > 0) && (j < order.size());
        final Integer temp = (Integer) order.get(index);
        order.set(index, order.get(j));
        order.set(j, temp);
    }

    private static void putIndicesToSet(IndexedHashMap map, final Integer firstId, final HashSet secondIds)
    {
        if(map.containsKey(firstId))
        {
            HashSet tempHashSet = (HashSet) map.get(firstId);
            tempHashSet.addAll(secondIds);
        }
        else
        {
            HashSet newHashSet = new HashSet();
            newHashSet.addAll(secondIds);
            map.put(firstId, newHashSet);
        }
    }

    private static boolean shouldParseLine(String strLine)
    {
        return !StringUtil.isEmpty(strLine);
    }

    public static void main(String[] args)
    {
        PagesDblComparator comparator;

        if(args.length < 4)
        {
            System.err.println("PagesDblComparator\nUsage: java PagesDblComparator output.log yandexDblPages.log yandexIds.log vinitiDblPages.log vinitiIds.log\n");
            return;
        }

        try
        {
            comparator = new PagesDblComparator();
            comparator.makeStatistics(args);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(DataFormatException e)
        {
            e.printStackTrace();
        }
    }
}