/*
 * This program is a part of the Darmstadt JSM Implementation.
 *
 * You can redistribute it (modify it, compile it, decompile it, whatever)
 * AMONG THE JSM COMMUNITY. If you plan to use this program outside the
 * community, please notify V.K.Finn (finn@viniti.ru) and the authors.
 *
 * Authors: Peter Grigoriev and Serhiy Yevtushenko
 * E-mail: {peter, sergey}@intellektik.informatik.tu-darmstadt.de
 *
 * Date: 07.10.2003
 * Time: 13:45:31
 */

package util;

import java.io.*;
import java.net.*;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.*;

public class ResourceLoader
{
    private static ResourceLoader m_resourceLoader;

    private ResourceLoader()
    {
    }

    public static synchronized ResourceLoader instance()
    {
        if (m_resourceLoader == null)
        {
            m_resourceLoader = new ResourceLoader();
        }

        return m_resourceLoader;
    }

    public byte[] getBytesFromResourceLocation(String resourceLocation)
    {
        if (resourceLocation == null)
        {
            return null;
        }

        // for file resource loacation
        if( resourceLocation.startsWith("..") || resourceLocation.startsWith("/") ||
            resourceLocation.startsWith("\\") ||
            ((resourceLocation.length() > 1) && (resourceLocation.charAt(1) == ':')))
        {
            return getBytesFromFile(resourceLocation);
        }

        URL urlLocation = ClassLoader.getSystemResource(resourceLocation);

        if (urlLocation == null)
        {
            // try again for web start applications
            urlLocation = ClassLoader.getSystemResource(resourceLocation);
        }

        if (urlLocation == null)
        {
            return null;
        }

        String locationString = urlLocation.toString();

        int posJAR = locationString.indexOf(".jar!");
        int posZIP = locationString.indexOf(".zip!");
        int pos = -1;

        if ((posJAR > -1) && (posZIP > -1))
        {
            pos = Math.min(posJAR, posZIP);
        }
        else
        {
            if (posJAR > -1)
            {
                pos = posJAR;
            }
            else
            {
                if (posZIP > -1)
                {
                    pos = posZIP;
                }
            }
        }

        // is the resource file in a zip or a jar file
        if (pos > -1)
        {
            String urlToZip = locationString.substring(4, pos + 4);
            String internalArchivePath = locationString.substring(pos + 6);
            return getBytesFromArchive(urlToZip, internalArchivePath);
        }
        else
        {
            String fileLocation = urlLocation.getFile();
            Pattern spacePattern = Pattern.compile("%20");
            Matcher matcherForSpacePattern = spacePattern.matcher(fileLocation);
            locationString = matcherForSpacePattern.replaceAll(" ");
            return getBytesFromFile(locationString);
        }
    }

    public static Vector readLines(String resourceFile)
    {
        return readLines(resourceFile, false);
    }

    public static Vector readLines(String resourceFile, boolean ignoreCommentedLines)
    {
        if (resourceFile == null)
        {
            return null;
        }

        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(resourceFile);

        if (bytes == null)
        {
            return null;
        }

        ByteArrayInputStream inputStreamBytes = new ByteArrayInputStream(bytes);
        LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(inputStreamBytes));

        String currentLine;
        Vector result = new Vector(100);

        try
        {
            while ((currentLine = lineNumberReader.readLine()) != null)
            {
                if (!ignoreCommentedLines)
                {
                    if (!(currentLine.charAt(0) == '#'))
                    {
                        result.add(currentLine);
                    }
                }
                else
                {
                    result.add(currentLine);
                }
            }
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
        }

        return result;
    }

    private byte[] getBytesFromArchive(String urlToZipArchive, String internalArchivePath)
    {
        URL url = null;
        int sizeOfArchive = -1;
        byte[] readBytes = null;

        try
        {
            url = new URL(urlToZipArchive);

            ZipFile zipFile = new ZipFile(url.getFile());
            Enumeration zipFileEnumeration = zipFile.entries();

            while (zipFileEnumeration.hasMoreElements())
            {
                ZipEntry zipEntry = (ZipEntry) zipFileEnumeration.nextElement();

                if (zipEntry.getName().equals(internalArchivePath))
                {
                    if (zipEntry.isDirectory())
                    {
                        return null;
                    }

                    if (zipEntry.getSize() > 65536)
                    {
                        System.out.println("Resource files should be smaller than 65536 bytes");
                    }

                    sizeOfArchive = (int) zipEntry.getSize();
                }
            }

            zipFile.close();

            FileInputStream fileInputStream = new FileInputStream(url.getFile());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            ZipInputStream zipInputStream = new ZipInputStream(bufferedInputStream);
            ZipEntry zipEntry = null;

            while ((zipEntry = zipInputStream.getNextEntry()) != null)
            {
                if (zipEntry.getName().equals(internalArchivePath))
                {
                    readBytes = new byte[sizeOfArchive];

                    int currentPosition = 0;
                    int chunk = 0;

                    while ((sizeOfArchive - currentPosition) > 0)
                    {
                        chunk = zipInputStream.read(readBytes, currentPosition, sizeOfArchive - currentPosition);

                        if (chunk == -1)
                        {
                            break;
                        }

                        currentPosition += chunk;
                    }
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }

        return readBytes;
    }

    private byte[] getBytesFromFile(String filePath)
    {
        if (filePath.startsWith("/cygdrive/"))
        {
            int length = "/cygdrive/".length();
            filePath = filePath.substring(length, length + 1) + ":" + filePath.substring(length + 1);
        }

        File file = new File(filePath);
        FileInputStream fileInputStream = null;

        try
        {
            fileInputStream = new FileInputStream(file);
        }
        catch (Exception e)
        {
            return null;
        }

        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

        int fileSize = (int) file.length();
        byte[] readBytes = new byte[fileSize];
        int currentPosition = 0;
        int chunk = 0;

        try
        {
            while ((fileSize - currentPosition) > 0)
            {
                chunk = bufferedInputStream.read(readBytes, currentPosition, fileSize - currentPosition);

                if (chunk == -1)
                {
                    break;
                }

                currentPosition += chunk;
            }
        }
        catch (Exception e)
        {
            return null;
        }

        return readBytes;
    }
}