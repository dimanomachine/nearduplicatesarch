package util;

public class Stopwatch {

    static final int SECONDS_PER_MINUTE = 60;
    static final int MINUTES_PER_HOUR = 60;
    static final int SECONDS_PER_HOUR = 3600;
    static final String MINUTES_MARK = " min. ";
    static final String HOURS_MARK = " hours ";
    static final String SECONDS_MARK = " sec.";
    private long m_nStartTime, m_nPreviousTime;

    public Stopwatch() {
        m_nStartTime = 0L;
        m_nPreviousTime = 0L;
        restart();
    }

    public long getTotalTime() {
        return getCurrentTime() - getStartTime();
    }

    public long getDelayTime() {
        long l = getCurrentTime() - m_nPreviousTime;
        m_nPreviousTime = getCurrentTime();
        return l;
    }

    public String getDelayAsString() {
        long l = getDelayTime();
        if(l < 60L)
            return "" + l + " sec.";
        if(l < 3600L) {
            long l1 = getMinutes(l);
            long l3 = getSeconds(l);
            return "" + l1 + " min. " + l3 + " sec.";
        } else {
            long l2 = getHours(l);
            long l4 = getMinutes(l);
            long l5 = getSeconds(l);
            return "" + l2 + " hours " + l4 + " min. " + l5 + " sec.";
        }
    }

    public static long getSeconds(long l) {
        return l % 60L;
    }

    public static long getMinutes(long l) {
        return (l % 3600L) / 60L;
    }

    public static long getHours(long l) {
        return l / 3600L;
    }

    public void restart() {
        m_nStartTime = getCurrentTime();
        m_nPreviousTime = m_nStartTime;
    }

    public String getReport() {
        return " [total time " + getTotalTime() + " seconds] [delay " + getDelayTime() + " seconds]";
    }

    private static long getCurrentTime() {
        return System.currentTimeMillis();// / 1000L;
    }

    private long getStartTime() {
        return m_nStartTime;
    }
}
