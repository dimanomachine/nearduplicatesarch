package util;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.*;

public class StringUtil
{
    public static String formatPercents(double val)
    {
        return DecimalFormat.getPercentInstance().format(val);
    }


    public static String formatPercents(double val, int points)
    {
        if (points < 0)
        {
            throw new IllegalArgumentException(" precision should be positive " + points);
        }
        if (points == 0)
        {
            return formatPercents(val);
        }
        StringBuffer pattern = new StringBuffer(((DecimalFormat) DecimalFormat.getPercentInstance()).toPattern());
        pattern.append(".");
        for (int i = 0; i < points; i++)
        {
            pattern.append("#");
        }
        return new DecimalFormat(pattern.toString()).format(val);
    }


    public static String getExtension(String path)
    {
        path = safeTrim(path);
        int dotPos = path.lastIndexOf('.');
        if ('/' == File.separatorChar)
        {
            path = path.replace('\\', File.separatorChar);
        }
        else
        {
            path = path.replace('/', File.separatorChar);
        }
        int sepPos = path.lastIndexOf(File.separatorChar);

        if (sepPos >= dotPos)
        {
            return "";
        }
        return path.substring(dotPos + 1);
    }


    public static String stackTraceToString(Throwable ex)
    {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw, true));
        return sw.toString();
    }


    /**
     * Insert the method's description here.
     * Creation date: (10.02.01 0:07:15)
     * @return java.lang.String
     * @param s java.lang.String
     */
    public static String safeTrim(String s)
    {
        if (null == s)
        {
            return "";
        }
        return s.trim();
    }

    public static boolean isEmpty(String toCheck)
    {
        String temp = safeTrim(toCheck);
        if ("".equals(temp))
        {
            return true;
        }
        return false;
    }

    public static String safeText(String text)
    {
        if (isEmpty(text))
        {
            return "";
        }
        return text;
    }

    public static String replaceStringWithNewString(String toProcess, String toReplace, String replaceWith)
    {
        int currPos = toProcess.indexOf(toReplace, 0);
        if (currPos == -1)
        {
            return toProcess;
        }

        StringBuffer ret = new StringBuffer();
        int prevPos = 0;

        do
        {
            ret.append(toProcess.substring(prevPos, currPos));
            ret.append(replaceWith);

            prevPos = currPos + toReplace.length();
            currPos = toProcess.indexOf(toReplace, prevPos);
        }
        while (currPos != -1);

        ret.append(toProcess.substring(prevPos));

        return ret.toString();
    }

    public static String extractClassName(String className)
    {
        int indexOfDot = className.lastIndexOf('.');
        if (indexOfDot > 0)
        {
            if (indexOfDot < className.length() - 1)
            {
                return className.substring(indexOfDot + 1);
            }
        }
        return className;
    }

    public static List split(String str, String delim)
    {
        StringTokenizer tokenizer = new StringTokenizer(str, delim);
        ArrayList result = new ArrayList();
        while (tokenizer.hasMoreTokens())
        {
            String nextToken = tokenizer.nextToken();
            if (!StringUtil.isEmpty(nextToken))
            {
                result.add(nextToken);
            }
        }
        return result;
    }

    public static String join(Collection collection, String delim)
    {
        StringBuffer ret = new StringBuffer();
        join(ret, collection, delim);
        return ret.toString();
    }

    public static void join(StringBuffer buf, Collection collection, String delim)
    {
        Iterator iter = collection.iterator();
        boolean first = true;
        while (iter.hasNext())
        {
            Object str = iter.next();
            if (first)
            {
                first = false;
            }
            else
            {
                buf.append(delim);
            }
            buf.append(str);
        }
    }


    public static void join(StringBuffer buf, int[] array, String delim)
    {
        joinArray(buf, array, delim);
    }

    public static String join(String[] array, String delim)
    {
        StringBuffer buf = new StringBuffer();
        joinArray(buf, array, delim);
        return buf.toString();
    }

    private static void joinArray(StringBuffer buf, Object array, String delim)
    {
        int length = Array.getLength(array);
        if (length == 0)
        {
            return;
        }
        buf.append(Array.get(array, 0));
        for (int i = 1; i < length; i++)
        {
            buf.append(delim);
            buf.append(Array.get(array, i));
        }
    }


    public static String findASubstring(String string, String[] substrings)
    {
        for (int i = 0; i < substrings.length; i++)
        {
            String substring = substrings[i];
            if (string.indexOf(substring) != -1)
            {
                return substring;
            }
        }
        return null;
    }

    public static boolean isFrequentSubstring(String small, String big, int nMinOcccurences)
    {
        return isFrequentSubstring(small, big, nMinOcccurences, 0);
    }

    public static boolean isFrequentSubstring(String small, String big, int nMinOcccurences, int nFromIndex)
    {
        int nOcc = 0;
        for (int nIndex = big.indexOf(small, nFromIndex); nIndex != -1; nIndex = big.indexOf(small))
        {
            nOcc++;
            if (nOcc >= nMinOcccurences)
            {
                return true;
            }
            big = big.substring(nIndex + small.length());
        }
        return false;
    }

    public static String joinArray(Object array, String delim)
    {
        StringBuffer result = new StringBuffer();
        joinArray(result, array, delim);
        return result.toString();
    }

    public static int getNumOccurences(final String string, char ch)
    {
        int nOccurences = 0;
        final int length = string.length();
        for (int i = 0; i < length; i++)
        {
            if (ch == string.charAt(i))
            {
                nOccurences++;
            }
        }
        return nOccurences;
    }

    public static int getNumOccurences(final String string, final String substring)
    {
        int nOccurences = 0;
        final int length = string.length();
        final int substringLength = substring.length();
        for (int i = 0; i < length - substringLength; i++)
        {
            if (substring.equals(string.substring(i, i + substringLength)))
            {
                nOccurences++;
            }
        }
        return nOccurences;
    }

    public static double getFrequency(final String string, String substring)
    {
        return computeFrequency(string, getNumOccurences(string, substring));
    }

    public static double getFrequency(final String string, char ch)
    {
        return computeFrequency(string, getNumOccurences(string, ch));
    }

    private static double computeFrequency(final String string, int nOccurences)
    {
        final int length = string.length();
        if (0 == length)
        {
            return 0;
        }
        return (double) nOccurences / (double) length;
    }

    public static boolean containsIgnoreCase(String big, String small)
    {
        if (isEmpty(small))
        {
            return true;
        }
        if (isEmpty(big))
        {
            return false;
        }
        return big.toUpperCase().indexOf(small.toUpperCase()) != -1;
    }

    public static boolean equal(String s1, String s2)
    {
        if (s1 == s2)
        {
            return true;
        }
        if (s1 == null)
        {
            return false;
        }

        return s1.equals(s2);
    }

    public static boolean isPrefix(String small, String big)
    {
        return isPrefixAt(small, big, 0);
    }

    public static boolean isPrefixAt(String small, String big, int nReadPos)
    {
        if (isEmpty(small))
        {
            return true;
        }
        if (isEmpty(big))
        {
            return false;
        }
        final int smallLength = small.length();
        final int bigLength = big.length() - nReadPos;
        if (smallLength > bigLength)
        {
            return false;
        }
        for (int i = 0; i < smallLength; i++)
        {
            if (small.charAt(i) != big.charAt(nReadPos + i))
            {
                return false;
            }
        }

        return true;
    }

    public static boolean isSuffix(String small, String big)
    {
        if (isEmpty(small))
        {
            return true;
        }
        if (isEmpty(big))
        {
            return false;
        }
        final int smallLength = small.length();
        final int bigLength = big.length();
        if (smallLength > bigLength)
        {
            return false;
        }
        final int lastSmallIndex = smallLength - 1;
        final int lastBigIndex = bigLength - 1;
        for (int i = 0; i < smallLength; i++)
        {
            if (small.charAt(lastSmallIndex - i) != big.charAt(lastBigIndex - i))
            {
                return false;
            }
        }

        return true;
    }

    public static boolean isSurrounded(String s, String prefix, String suffix)
    {
        if (!isPrefix(prefix, s))
        {
            return false;
        }
        if (!isSuffix(suffix, s))
        {
            return false;
        }
        return true;
    }

    public static String findWrappedSubstring(String s, String prefix, String suffix)
    {
        int nStartPos = s.indexOf(prefix);
        if (-1 == nStartPos)
        {
            return null;
        }
        nStartPos += prefix.length();
        int nEndPos = s.indexOf(suffix, nStartPos);
        if (-1 == nEndPos)
        {
            return null;
        }

        return s.substring(nStartPos, nEndPos);
    }
}
