package util.collection;

import java.util.Map;

public interface IndexedMap extends Map {
    public abstract Object getKey(int i);
}
