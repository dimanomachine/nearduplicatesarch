/*
 * User: Serhiy Yevtushenko
 * Date: Sep 4, 2002
 * Time: 2:35:11 AM
 */
package util.collection;

import util.collection.IndexedMap;

import java.util.LinkedHashMap;
import java.util.Map;

public class IndexedHashMap extends LinkedHashMap implements IndexedMap{
    public IndexedHashMap(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public IndexedHashMap(int initialCapacity) {
        super(initialCapacity);
    }

    public IndexedHashMap() {
    }

    public IndexedHashMap(Map m) {
        super(m);
    }

    Object[] keyArray;

    public void clear() {
        keyArray = null;
        super.clear();
    }

    public Object put(Object key, Object value) {
        keyArray = null;
        return super.put(key, value);
    }

    public Object remove(Object key) {
        keyArray = null;
        return super.remove(key);
    }

    public Object getKey(int index) {
        if(keyArray == null){
            keyArray= keySet().toArray();
        }
        return keyArray[index];
    }
}
