package util.comparators;

import java.util.Comparator;
import java.util.Date;

public class ComparatorUtil {
    public static int compareDoubles(double d1, double d2) {
        return sign(d1 - d2);
    }

    public static int compareInt(int i1, int i2) {
        return sign(i1 - i2);
    }

    public static int compareBoolean(boolean b1, boolean b2) {
        if (b1 == b2) {
            return 0;
        } else if (b1) { // Define false < true
            return 1;
        } else {
            return -1;
        }
    }

    public static int compareStrings(String s1, String s2) {
        return sign(s1.compareTo(s2));
    }

    public static int compareStringIgnoreCase(String s1, String s2){
        return sign(s1.compareToIgnoreCase(s2));
    }

    public static int compareDates(Date d1, Date d2) {
        return sign(d1.getTime() - d2.getTime());
    }

    public static int defaultCompareObjects(Object o1, Object o2) {
        return compareStrings(String.valueOf(o1), String.valueOf(o2));
    }

    private static int sign(double result) {
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    static public class BooleanComparator implements Comparator{
        private BooleanComparator(){}

        private static final Comparator g_Instance = new BooleanComparator();

        public static Comparator getInstance(){
            return g_Instance;
        }

        public int compare(Object o1, Object o2) {
            return compareBoolean(
                    ((Boolean)o1).booleanValue(),
                    ((Boolean)o2).booleanValue()
            );
        }
    }

    static public class DateComparator implements Comparator{
        private DateComparator() {
        }

        private static final Comparator g_Instance = new DateComparator();

        public static Comparator getInstance(){
            return g_Instance;
        }

        public int compare(Object o1, Object o2) {
            return compareDates(
                    ((Date)o1),
                    ((Date)o2)
            );
        }
    }

    static public class StringComparator implements Comparator{
        private StringComparator() {
        }

        private static final Comparator g_Instance = new StringComparator();

        public static Comparator getInstance(){
            return g_Instance;
        }


        public int compare(Object o1, Object o2) {
            return compareStrings(
                    String.valueOf(o1),
                    String.valueOf(o2)
            );
        }
    }

     static public class StringComparatorIgnoreCase implements Comparator{
        private StringComparatorIgnoreCase() {
        }

        private static final Comparator g_Instance = new StringComparatorIgnoreCase();

        public static Comparator getInstance(){
            return g_Instance;
        }


        public int compare(Object o1, Object o2) {
            return compareStringIgnoreCase(
                    String.valueOf(o1),
                    String.valueOf(o2)
            );
        }
    }

    static public class DefaultObjectComparator implements Comparator{
        private DefaultObjectComparator() {
        }

        private static final Comparator g_Instance = new DefaultObjectComparator();

        public static Comparator getInstance(){
            return g_Instance;
        }

        public int compare(Object o1, Object o2) {
            return defaultCompareObjects(o1, o2);
        }
    }

    static public class NumberComparator implements Comparator {
        private NumberComparator() {
        }

        private static final Comparator g_Instance = new NumberComparator();

        public static Comparator getInstance(){
            return g_Instance;
        }

        public int compare(Object o1, Object o2) {
            return compareDoubles(((Number)o1).doubleValue(),
                    ((Number)o2).doubleValue());
        }

    }
}