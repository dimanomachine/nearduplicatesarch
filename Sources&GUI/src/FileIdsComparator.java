
import util.ResourceLoader;
import util.StringUtil;
import util.Stopwatch;
import util.collection.IndexedHashMap;

import java.util.StringTokenizer;
import java.util.zip.DataFormatException;
import java.io.*;

public class FileIdsComparator
{
    private static final String DESCRIPTION_DELIMITER = " ";
    private static final int HASHMAP_INITIAL_CAPACITY = 32;

    private IndexedHashMap m_yandexIdsMap = new IndexedHashMap(HASHMAP_INITIAL_CAPACITY);
    private IndexedHashMap m_vinitiIdsMap = new IndexedHashMap(HASHMAP_INITIAL_CAPACITY);

    private IndexedHashMap m_hashToUrlMap = new IndexedHashMap(HASHMAP_INITIAL_CAPACITY);
    private boolean m_isUrlLoaded = false;

    private PrintWriter m_pwFileIds;

    FileIdsComparator()
    {
    }

    public IndexToIndexMap mapIndices(final String firstUrl, final String secondUrl)
    {
        IndexToIndexMap indexToIndexMap = new IndexToIndexMap();

        try
        {
            load(firstUrl, m_yandexIdsMap);
            m_isUrlLoaded = true;
            load(secondUrl, m_vinitiIdsMap);

            for(int i = 0; i < m_vinitiIdsMap.size(); i ++)
            {
                final Object key = m_vinitiIdsMap.getKey(i);
                final Integer yandexIndex = (Integer) m_yandexIdsMap.get(key);
                if(null != yandexIndex)
                {
                    final Integer vinitiIndex = (Integer) m_vinitiIdsMap.get(key);
                    indexToIndexMap.put(yandexIndex, vinitiIndex);
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(DataFormatException e)
        {
             e.printStackTrace();
        }

        return indexToIndexMap;
    }

    private IndexToIndexMap compareFiles(String[] args) throws IOException, DataFormatException
    {
        m_pwFileIds = new PrintWriter(new FileWriter(args[0]), true);

        String yandexRepositoryUrl = args[1];
        String vinitiRepositoryUrl[] = new String[args.length - 2];

        for(int i = 2; i < args.length; i++)
        {
            vinitiRepositoryUrl[i - 2] = args[i];
        }

        Stopwatch stopwatch = new Stopwatch();

        load(yandexRepositoryUrl, m_yandexIdsMap);
        System.out.println(">> yandex file-ids (size = " + m_yandexIdsMap.size() + "): " + yandexRepositoryUrl + " loaded in " + stopwatch.getTotalTime() + " ms.");
        m_isUrlLoaded = true;
        stopwatch.restart();

        for(int i = 0; i < vinitiRepositoryUrl.length; i ++)
        {
            load(vinitiRepositoryUrl[i], m_vinitiIdsMap);
            System.out.println(">> viniti file-ids #" + (i + 1) + " (size = " + m_vinitiIdsMap.size() + "): " + vinitiRepositoryUrl[i] + " loaded in " + stopwatch.getTotalTime() + " ms.");
            stopwatch.restart();
        }

        IndexToIndexMap indexToIndexMap = new IndexToIndexMap();

        m_pwFileIds.println("yandexId" + DESCRIPTION_DELIMITER + "vinitiId" + DESCRIPTION_DELIMITER + "URL");
        for(int i = 0; i < m_vinitiIdsMap.size(); i++)
        {
            final Object key = m_vinitiIdsMap.getKey(i);
            final Integer yandexIndex = (Integer) m_yandexIdsMap.get(key);
            if(null != yandexIndex)
            {
                final Integer vinitiIndex = (Integer) m_vinitiIdsMap.get(key);
                m_pwFileIds.println(yandexIndex + DESCRIPTION_DELIMITER + vinitiIndex + DESCRIPTION_DELIMITER + m_hashToUrlMap.get(key));
                indexToIndexMap.put(yandexIndex, vinitiIndex);
            }
        }
        System.out.println(">> yandex and viniti file-ids compared in " + stopwatch.getTotalTime() + " ms.");

        return indexToIndexMap;
    }

    public void load(String url, IndexedHashMap map) throws IOException, DataFormatException
    {
        byte[] bytes = ResourceLoader.instance().getBytesFromResourceLocation(url);
        ByteArrayInputStream inputStreamBytes = null;
        InputStreamReader inputStreamReader = null;

        try
        {
            if(bytes != null)
            {
                inputStreamBytes = new ByteArrayInputStream(bytes);
                inputStreamReader = new InputStreamReader(inputStreamBytes);
                read(inputStreamReader, map);
            }
            else
            {
                throw new IOException("Incorrect path: " + url);
            }
        }
        finally
        {
            if(null != inputStreamBytes)
            {
                inputStreamBytes.close();
                inputStreamBytes = null;
            }

            if(null != inputStreamReader)
            {
                inputStreamReader.close();
                inputStreamReader = null;
            }
        }
    }

    private void read(InputStreamReader r, IndexedHashMap map) throws IOException, DataFormatException
    {
        LineNumberReader reader = new LineNumberReader(r);
        for(String strLine = reader.readLine(); null != strLine; strLine = reader.readLine())
        {
            strLine = strLine.trim();
            if(shouldParseLine(strLine))
            {
                final StringTokenizer oTokenizer = new StringTokenizer(strLine, DESCRIPTION_DELIMITER);

                if(oTokenizer.countTokens() < 2)
                {
                    throw new DataFormatException("Illegal file description: " + strLine);
                }

                String fileUrl = oTokenizer.nextToken();
                String fileId = oTokenizer.nextToken();

                final Integer urlHash = new Integer (fileUrl.hashCode());
                map.put(urlHash, Integer.decode(fileId));
                if(!m_isUrlLoaded)
                {
                    m_hashToUrlMap.put(urlHash, fileUrl);
                }
            }
        }

        if(null != reader)
        {
            reader.close();
        }
    }

    private static boolean shouldParseLine(String strLine)
    {
        return !StringUtil.isEmpty(strLine);
    }

    public static void main(String[] args)
    {
        FileIdsComparator comparator;

        if(args.length < 3)
        {
            System.err.println("FileIdsComparator\nUsage: java FileIdsComparator output.log yandexIds.log vinitiIds.log [vinitiIds2.log [viniitiIds3.log] ...]\n");
            return;
        }

        try
        {
            comparator = new FileIdsComparator();
            comparator.compareFiles(args);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        catch(DataFormatException e)
        {
            e.printStackTrace();
        }
    }
}