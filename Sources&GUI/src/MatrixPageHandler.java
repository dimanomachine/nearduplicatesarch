
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;
import java.util.Vector;

/*
 * Created on May 19, 2005
 */

/**
 * @author object
 */
public class MatrixPageHandler extends BaseShinglePageHandler
{

    private static class FixedSizeDocumentRepresentation
    {
        FixedSizeDocumentRepresentation(final Vector vMatrices)
        {
            m_vMatrices = vMatrices;
            m_arrFingeprints = new int[m_vMatrices.size()];
        }

        void add(final int nFingerprint)
        {
            for(int i = 0; i < m_arrFingeprints.length; i++)
            {
                final int nPermutation = ((BitMatrix) m_vMatrices.get(i)).timesVector(nFingerprint);
                if(nPermutation < m_arrFingeprints[i] || m_fFresh)
                {
                    m_arrFingeprints[i] = nPermutation;
                }
            }
            m_fFresh = false;
        }

        int get(final int i)
        {
            return m_arrFingeprints[i];
        }

        private final Vector m_vMatrices;
        private final int[] m_arrFingeprints;
        private boolean m_fFresh = true;
    }

    public void initialize(Hashtable args) throws IOException
    {
        super.initialize(args);
        final String strShinglesPerDoc = (String) args.get(SHINGLES_PER_DOC_PARAMETER);
        if(null != strShinglesPerDoc)
        {
            m_nShinglesPerDoc = Integer.parseInt(strShinglesPerDoc);
        }
        
        initializeMatrices();
    }

	private void initializeMatrices() throws IOException {
		final String strIdsFolder = getIdsFolder();
		assert null != strIdsFolder;
		
		File file = new File(strIdsFolder, MATRIX_FILE);
		if (file.exists()) {
			loadMatrices(file);
		} else {
			generateMatrices(file);
		}
	}

	/**
	 * @param file
	 */
	private void generateMatrices(File file) throws IOException {
		System.out.println("Generating new matrices and saving them to " + file.getAbsolutePath());
		m_vMatrices = new Vector(m_nShinglesPerDoc);
		PrintWriter writer = new PrintWriter(new FileWriter(file), true);
		for(int i = 0; i < m_nShinglesPerDoc; i++)
		{
			BitMatrix bm = BitMatrix.random(Integer.SIZE);
			m_vMatrices.add(bm);
			System.out.println(bm);
			writer.println(bm);
		}
		writer.close();
	}

	private void loadMatrices(File file) throws IOException {
		System.out.println("Loading matrices from " + file.getAbsolutePath());
		m_vMatrices = new Vector();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		for (String strLine = reader.readLine(); null != strLine; strLine = reader.readLine()) {
			m_vMatrices.add(BitMatrix.fromString(strLine));
		}
		m_nShinglesPerDoc = m_vMatrices.size();
	}

	public void handlePage(String docID, String pageData)
    {
        super.handlePage(docID, pageData);
        final String strDocNo = registerDoc(docID);

        pageData = strip(pageData);

        FixedSizeDocumentRepresentation doc = new FixedSizeDocumentRepresentation(m_vMatrices);
        String strShingle = getNextShingle(pageData);
        while(null != strShingle)
        {
            doc.add(getHash(strShingle));
            strShingle = getNextShingle(pageData);
        }

        for(int i = 0; i < m_nShinglesPerDoc; i++)
        {
            registerFingerprint(strDocNo, doc.get(i), i);
        }

    }

    private int m_nShinglesPerDoc = 100;
    private Vector m_vMatrices;
    
    private final String MATRIX_FILE = "matrices.txt";
}
