import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Vector;

/*
 * Created on Jun 20, 2005
 *
 */

/**
 * @author object
 *
 */
public class Merger {
	public static void merge(File[] arrFiles, final String strOut) throws IOException {
		new Merger(arrFiles, strOut).merge();
	}
	
	private Merger(File[] arrFiles, final String strOut) throws IOException
	{
		if (null == arrFiles || null == strOut) {
			throw new IllegalArgumentException();
		}
		m_writer = new PrintWriter(strOut);
		
		m_readers = new Vector(arrFiles.length);
		for (int i = 0; i < arrFiles.length; i++) {
			DocumentFingerprintReader reader = new DocumentFingerprintReader(arrFiles[i]);
			if (!reader.hasFinished()) {
				m_readers.add(i, reader);
			}
		}
	}
	
	private void merge() throws IOException
	{
		while (m_readers.size() > 0) {
			m_minReader = (DocumentFingerprintReader) Collections.min(m_readers);
			m_minReader.readRecord(m_curRecord);

			assert null != m_curRecord;
			if (0 == m_curRecord.compareTo(m_firstRecord)) {
				appendCurrentRecord();
	               /**/		} else {
				startNewRecord();
				appendCurrentRecord();
			}

			if (m_minReader.hasFinished()) {
				m_readers.remove(m_minReader);
			}
		}
		m_writer.close();
	}

	private void appendCurrentRecord() {
		m_curRow.append(m_curRecord.getDocument());
		m_curRow.append(DOCUMENT_DELIMITER);
		m_nRecordCount++;
	}
	
	private void startNewRecord()
	{
		if (m_nRecordCount > 1) {
			m_writer.println(m_curRow);
		}
		m_curRow.setLength(0);
		m_nRecordCount = 0;
		m_firstRecord.copy(m_curRecord);
	}
	
	private final PrintWriter m_writer;
	private final Vector m_readers;
	private final DocumentFingerprintRecord m_firstRecord = new DocumentFingerprintRecord();
	private final DocumentFingerprintRecord m_curRecord = new DocumentFingerprintRecord();
	private DocumentFingerprintReader m_minReader;
	private StringBuffer m_curRow = new StringBuffer();
	private int m_nRecordCount = 0;
	
	private static final String DOCUMENT_DELIMITER = " ";
}

class DocumentFingerprintReader implements Comparable
{
	DocumentFingerprintReader(final File file) throws IOException
	{
		m_brReader = new BufferedReader(new FileReader(file));
		readNextRecord();
	}

	public int compareTo(Object obj) {
		assert obj instanceof DocumentFingerprintReader;		
		DocumentFingerprintReader reader = (DocumentFingerprintReader) obj;
		
		assert null != m_record;
		assert null != reader.m_record;
		return m_record.compareTo(reader.m_record);
	}
	
	public void readRecord(DocumentFingerprintRecord record) throws IOException {
		record.copy(m_record);
		readNextRecord();
	}
	
	public boolean hasFinished()
	{
		return m_fFinished;
	}
	
	private void readNextRecord() throws IOException
	{
		final String strRecord = m_brReader.readLine();
		if (null == strRecord) {
			m_fFinished = true;
		} else {
			m_record.loadFromString(strRecord);
		}
	}

	private boolean m_fFinished = false;
	private final BufferedReader m_brReader;
	private final DocumentFingerprintRecord m_record = new DocumentFingerprintRecord();
}
