import java.util.Hashtable;

/**
 *  Sample implementation of ROMIP's PageHandler interface.
 */

class SimplePageHandler implements PageHandler {
	public void initialize(Hashtable args) {
	}
	public void handlePage(String docID, String pageData) {
		System.out.println("id=" + docID + " len=" + pageData.length());
		//    System.out.println(pageData);
	}
	public boolean startNewFile(String strFilePath) {
		return true;
	}
}
