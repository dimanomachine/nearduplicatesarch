/*
 * Created on Jun 13, 2005
 *
 */

/**
 * @author object
 *
 */
class DocumentFingerprintRecord implements Comparable {

	DocumentFingerprintRecord() {
	}

	public DocumentFingerprintRecord(final String strRecord) {
		loadFromString(strRecord);
	}
	
	public void loadFromString(final String strRecord)
	{
		String[] arrParts = strRecord.split(BaseShinglePageHandler.DELIMITER);
		if (arrParts.length > 1) {
			m_nDocument = Integer.parseInt(arrParts[0]);
			m_nFingerprint = Integer.parseInt(arrParts[1]);
			if (SortAndMerge.isVectorMode()) {
				if (arrParts.length > 2) {
					m_nPosition = Integer.parseInt(arrParts[2]);
				} else {
					wrongFormat();
				}
			}
		} else {
			wrongFormat();
		}
	}
	
	public void copy(final DocumentFingerprintRecord record)
	{
		m_nDocument = record.m_nDocument;
		m_nFingerprint = record.m_nFingerprint;
		m_nPosition = record.m_nPosition;
	}
	
	public int compareTo(Object obj) {
		if (null == obj) {
			return 1;
		}
		
		assert obj instanceof DocumentFingerprintRecord;
		
		DocumentFingerprintRecord record = (DocumentFingerprintRecord) obj;
        if (m_nFingerprint<record.m_nFingerprint)
        {
            return -1;
        }else
        {
           if(m_nFingerprint==record.m_nFingerprint)
           {

               if( m_nPosition == record.m_nPosition)
               {
                return 0;
               }else{
                   if(m_nPosition<record.m_nPosition)
                   {
                       return -1;
                   }else{
                       return 1;
                   }

               }

           }else
           {
              return 1;
           }
        }
        

       /* if (m_nFingerprint>record.m_nFingerprint)
        {
            return 1;
        }else
        {
           if(m_nFingerprint==record.m_nFingerprint)
           {
                return 0;
           }else
           {
              return -1;
           }
        }
        */
		/*int nResult = m_nFingerprint - record.m_nFingerprint;
		if (0 == nResult) {
			nResult = m_nPosition - record.m_nPosition;
		}
		return nResult;
         */
	
     }
	
	public String toString()
	{
		return m_nDocument + BaseShinglePageHandler.DELIMITER + m_nFingerprint + BaseShinglePageHandler.DELIMITER + m_nPosition;
	}
	
	public int getDocument() {
		return m_nDocument;
	}

	private void wrongFormat() {
		throw new RuntimeException("This format is not supported");
	}
	
	private int m_nDocument;
	private int m_nFingerprint;
	private int m_nPosition;
}
