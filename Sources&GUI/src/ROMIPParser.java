import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

/**
 *  Main parser class for ROMIP dataset.
 *
 *  To customize parsing develop new class implementing PageHandler interface
 *  and pass it name as parameter on command line.
 *  See SimplePageHandler as an example.
 *
 *  Sample usage:
 *     java ROMIPParser SimplePageHandler
 */

public class ROMIPParser extends org.xml.sax.helpers.DefaultHandler {
	public final static String VERSION = "1.0";

	private PageHandler handler;
	private String docID;
	private StringBuffer encodedContent;
	private String currentTag;

	private ROMIPParser() {
	};

	ROMIPParser(PageHandler h) {
		handler = h;
		encodedContent = new StringBuffer(10000);
	}

	public void startElement(
		String namespaceURI,
		String localName,
		String qName,
		Attributes atts) {
		if (qName.equals("document")) {
			docID = "";
			encodedContent.setLength(0);
		}
		currentTag = qName;
	}

	public void characters(char[] ch, int start, int length) {
		if (currentTag != null) {
			if (currentTag.equals("docID")) {
				docID += new String(ch, start, length);
			} else if (currentTag.equals("content")) {
				encodedContent.append(ch, start, length);
			}
		}
	}

	public void endElement(
		String namespaceURI,
		String localName,
		String qName) {
		if (qName.equals("document")) {
			handler.handlePage(
				docID,
				Base64.decodeToString(encodedContent.toString()));
		}

		currentTag = null;
	}

	public static void main(String[] args) {
		PageHandler handler = null;

		prepareArgs(args);

		final String strPath = (String) s_hArgs.get(PATH);
		final String strHandlerClassName = (String) s_hArgs.get(HANDLER);
		if (null == strHandlerClassName || null == strPath) {
			System.err.println(
				"Official parser for ROMIP dataset (Version "
					+ VERSION
					+ ")\nUsage:\n  java ROMIPParser -in <input-path> -h <handler-class> [handler-arguments]\n");
			return;
		}

		try {
			final Class c = Class.forName(strHandlerClassName);

			assert c != null;
			handler = (PageHandler) c.newInstance();
			handler.initialize(s_hArgs);
		} catch (ClassNotFoundException e) {
			System.err.println("Failed to find handler class " + args[0]);
			return;
		} catch (Exception e) {
			System.err.println("Failed to instantiate handler class: \n" + e.getMessage());
			return;
		}

		try {
			final File f = new File(strPath);
			if (!f.exists()) {
				System.err.println("File not found " + strPath);
			} else {
				parse(
					f,
					SAXParserFactory.newInstance().newSAXParser(),
					new ROMIPParser(handler));
			}
		} catch (Exception e) {
			System.err.println(
				"Failed to setup parsing environment or parse input stream\n"
					+ e);
		}
	}

	private static void parse(
		final File f,
		final SAXParser sax,
		final ROMIPParser p) {
		assert f.exists();
		if (f.isDirectory()) {
			final File[] arrFiles = f.listFiles();
			for (int i = 0; i < arrFiles.length; i++) {
				parse(arrFiles[i], sax, p);
			}
		} else {
			try {
				if (p.handler.startNewFile(f.getName())) {
					sax.parse(f, p);
				} else {
					System.out.println("Ignoring " + f.getAbsolutePath());
				}
			} catch (IOException e) {
				printParseError(f.getAbsolutePath(), e.getMessage());
			} catch (SAXException e) {
				printParseError(f.getAbsolutePath(), e.getMessage());
			}
		}
	}

	private static void printParseError(
		final String strPath,
		final String strMessage) {
		System.err.println("Cannot parse " + strPath + ": " + strMessage);
	}

	private static void prepareArgs(final String[] args) {
		final int nLength = args.length - 1;
		for (int i = 0; i < nLength; i += 2) {
			s_hArgs.put(args[i], args[i + 1]);
		}
	}

	private static final Hashtable s_hArgs = new Hashtable();
	private static final String HANDLER = "-h";
	private static final String PATH = "-in";
}
