
/*
 * Created on May 19, 2005
 */

import java.util.Random;
import java.util.StringTokenizer;

/**
 * @author object
 */
class BitMatrix {
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < m_matrix.length; i++) {
			sb.append(m_matrix[i]);
			sb.append(SPACE);
		}
		return sb.toString();
	}
	
	int timesVector(final int nBits)
	{
		int nResult = 0;
        int nRows = Math.min(getRowDimension(), Integer.SIZE);
		for (int i = 0; i < nRows; i++) {
			nResult <<= 1;
			// each bit of m_matrix[i] & nBits is the product of the corresponding bits
			//      of nBits and the ith number in the matrix
			assert BitHelper.getBitSum(m_matrix[i] & nBits) == (Integer.bitCount(m_matrix[i] & nBits) % 2);
            nResult += BitHelper.getBitSum(m_matrix[i] & nBits);
		}

        return nResult;
	}
	
	int getRowDimension()
	{
		return m_matrix.length;
	}

	static int getColumnDimension()
	{
		return Integer.SIZE;
	}

	static BitMatrix random(final int nRows)
	{
		BitMatrix bm = new BitMatrix(nRows);
		bm.randomize();
		return bm;
	}
	
	static BitMatrix fromString(String str) {
		StringTokenizer st = new StringTokenizer(str);
		int nRows = st.countTokens();
		BitMatrix bm = new BitMatrix(nRows);
		for (int i = 0; i < nRows; i++) {
			assert st.hasMoreElements();
			bm.m_matrix[i] = Integer.parseInt(st.nextToken()); 
		}
		return bm;
	}
	
	private BitMatrix(int nRows) {
        m_matrix = new int[nRows];
	}
	
    private void randomize()
    {
		final Random generator = new Random();
		for(int i = 0; i < m_matrix.length; i ++)
		{
			m_matrix[i] = generator.nextInt(Integer.MAX_VALUE);
		}
    }
	
    private int[] m_matrix;

	private static final String SPACE = " ";
}
