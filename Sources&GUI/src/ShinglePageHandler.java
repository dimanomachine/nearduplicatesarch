
class ShinglePageHandler extends BaseShinglePageHandler implements PageHandler {
	public void handlePage(String docID, String pageData) {
		super.handlePage(docID, pageData);
		pageData = strip(pageData);
		final String strDocNo = registerDoc(docID);
		String strShingle = getNextShingle(pageData);
		int i = 0;
		while (null != strShingle) {
			registerShingle(strDocNo, strShingle, i++);
			strShingle = getNextShingle(pageData);
		}
	}
}
