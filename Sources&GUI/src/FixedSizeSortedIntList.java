/*
 * Created on May 8, 2005
 */

/**
 * @author object
 */
public class FixedSizeSortedIntList {
	static class IntNode
	{
		int getInt()
		{
			return m_nInt;
		}
		
		IntNode getNext()
		{
			return m_inNextNode;
		}
		
		IntNode(int i)
		{
			m_nInt = i;
		}
		
		void setNext(final IntNode node)
		{
			m_inNextNode = node;
		}
		
		private final int m_nInt;
		private IntNode m_inNextNode;
	}

	FixedSizeSortedIntList(final int nSize)
	{
		m_nSize = nSize;
	}
	
	void add(final int nInt)
	{
		if (null == m_inFirst) {
			addNode(nInt, null, null); 
		} else {
			IntNode prevNode = null;
			IntNode node = m_inFirst;
			for (int i = 0; i < m_nSize; i++) {
				if (null == node) {
					addNode(nInt, prevNode, node);
					break;
				} else if (nInt == node.getInt()) {
					break;
				} else if (nInt < node.getInt()) {
					node = addNode(nInt, prevNode, node);
					for (int j = i + 1; j < m_nSize && null != node; j++) {
						node = node.getNext();
					}
					if (null != node) {
						node.setNext(null);
					}
					break;
				} else {
					prevNode = node;
					node = node.getNext();
				}
			}
		}
	}
	
	IntNode getFirst()
	{
		return m_inFirst;
	}
	
	private IntNode addNode(final int nInt, final IntNode prevNode, final IntNode nextNode)
	{
		final IntNode node = new IntNode(nInt);
		if (null == prevNode) {
			m_inFirst = node;
		} else {
			prevNode.setNext(node);
		}
		node.setNext(nextNode);
		return node;
	}
	
	private final int m_nSize;
	private IntNode m_inFirst;
}
