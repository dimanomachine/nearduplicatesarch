/*
 * Created on May 26, 2005
 *
 */



class BitHelper
{
	static int getBitSum(int nBits)
	{
		final int nMaxPart = s_arrBitSums.length - 1;
		assert (nBits & nMaxPart) + (((nBits >>> BITS_PER_PART) & nMaxPart) << BITS_PER_PART)  == nBits;
		return s_arrBitSums[nBits & nMaxPart] ^ s_arrBitSums[(nBits >>> BITS_PER_PART) & nMaxPart];
	}
	
	private static byte computeBitSum(int n)
	{
		// returns 1 if n has an odd number of bits; 0, otherwise
		// this is the sum of bits in n
		assert n >= 0;
		int nSum = 0;
		while(n > 0)
		{
			nSum = 1 - nSum;
			n &= (n - 1);
		}
		assert 0 == n;
		return (byte)nSum;
	}
	
	private final static int BITS_PER_PART = Integer.SIZE / 2;
	private final static byte s_arrBitSums[] = new byte[1 << BitHelper.BITS_PER_PART];

	static
	{
		for(int i = 0; i < s_arrBitSums.length; i ++)
		{
			s_arrBitSums[i] = computeBitSum(i);
		}
	}
}