import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Vector;

/*
 * Created on Jun 19, 2005
 *
 */

/**
 * @author object
 *
 */
class Sorter {
	public static void sort(final File fileIn, final File fileOut) throws IOException {
		Vector vRecords = parseFile(fileIn);
		Collections.sort(vRecords);
		saveVector(vRecords, fileOut);
	}
	
	private static Vector parseFile(final File file) throws IOException {
		Vector vRecords = new Vector();
		final BufferedReader br = new BufferedReader(new FileReader(file));
		for (String strLine = br.readLine(); null != strLine; strLine = br.readLine()) {
			vRecords.add(new DocumentFingerprintRecord(strLine));
		}
		br.close();
		return vRecords;
	}
	
	private static void saveVector(Vector vRecords, File fileOut) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(fileOut);
		final int nSize = vRecords.size();
		for (int i = 0; i < nSize; i++) {
			pw.println(vRecords.get(i));
		}
		pw.close();
	}

}
