import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

/*
 * Created on Jun 2, 2005
 *
 */

/**
 * @author object
 *
 */
public class SortAndMerge {
	
	private static class SorterFileFilter implements FileFilter
	{
		public boolean accept(File pathname) {
			return pathname.isFile();
		}
	}

	public static void main(String[] args) {
		if (args.length > 1) {
			final File fIn = new File(args[0]);
			if (args.length > 2) {
				m_fVectorMode = VECTOR_PARAMETER.equals(args[2]);
			}
			if (fIn.exists()) {
				File dir = sort(fIn);
				if (null != dir) {
					try {
						merge(dir, args[1]);
					} catch (IOException e) {
						System.err.println(e.getMessage());
						help();
					}
				}
			} else {
				System.err.println(ERR_FILE_NOT_FOUND + fIn.getAbsolutePath());
				help();
			}
		} else {
			help();
		}
	}
	
	static boolean isVectorMode()
	{
		return m_fVectorMode;
	}

	private static void help() {
		System.err.println(USAGE);
	}

	private static File sort(final File fileIn) {
		System.out.println(SORTING);
		assert fileIn.exists();
		
		File[] arrFiles;
		if (fileIn.isDirectory()) {
			arrFiles = fileIn.listFiles(FILTER);
		} else {
			arrFiles = new File[1];
			arrFiles[0] = fileIn;
		}
		
		assert null != arrFiles;
		
		File tempDir = new File(TEMP_DIR);
		if (tempDir.mkdir()) {
			tempDir.deleteOnExit();
			for (int i = 0; i < arrFiles.length; i++) {
				File file = arrFiles[i];
				try {
					System.out.println(file.getAbsolutePath());
					final File fileOut = new File(tempDir, file.getName());
					fileOut.deleteOnExit();
					Sorter.sort(file, fileOut);
				} catch (IOException e) {
					System.err.println(ERR_SORTING + file.getAbsolutePath());
					final String strMessage = e.getMessage();
					if (null != strMessage) {
						System.err.println(strMessage);
					}
				}
			}
			System.out.print(SORTING);
			System.out.println(FINISHED);		
		} else {
			System.err.println(ERR_TEMP_DIRECTORY);
			tempDir = null;
		}
		
		return tempDir;
	}
	
	private static void merge(final File dir, final String strOutFile) throws IOException {
		System.out.println(MERGING);
		assert dir.isDirectory();
		
		Merger.merge(dir.listFiles(), strOutFile);
		
		System.out.print(MERGING);
		System.out.println(FINISHED);
	}
	
	private static boolean m_fVectorMode = false;
	
	private static SorterFileFilter FILTER = new SorterFileFilter();
	private static String TEMP_DIR = "temp";
	private static final String ERR_TEMP_DIRECTORY = "Cannot create temp directory";
	private static final String USAGE = "Usage: Sorter <input-path> <output-path>";
	private static final String ERR_FILE_NOT_FOUND = "File not found: ";
	private static final String ERR_SORTING = "Error while sorting ";
	
	private static final String VECTOR_PARAMETER = "v";
	private static final String SORTING = "SORTING";
	private static final String MERGING = "MERGING";
	private static final String FINISHED = " FINISHED";
}
