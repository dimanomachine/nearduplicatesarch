/*
 * Author: Samokhin Mikhail
 *
 * Date: 10.06.2005
 * Time: 17:28:37
 */

import util.collection.IndexedHashMap;

public class IndexToIndexMap
{
    public IndexToIndexMap()
    {
    }

    void put(final Integer yandexId, final Integer vinitiId)
    {
        m_yandex2vinitiIds.put(yandexId, vinitiId);
        m_viniti2yandexIds.put(vinitiId, yandexId);
    }

    Integer getYandex2VinitiIds(final Integer index)
    {
        return (Integer) m_yandex2vinitiIds.get(index);
    }

    Integer getViniti2YandexIds(final Integer index)
    {
        return (Integer) m_viniti2yandexIds.get(index);
    }

    boolean checkVinitiId(final Integer index)
    {
        return m_viniti2yandexIds.containsKey(index);
    }

    boolean checkYandexId(final Integer index)
    {
        return m_yandex2vinitiIds.containsKey(index);
    }

    IndexedHashMap m_yandex2vinitiIds = new IndexedHashMap(HASHMAP_INITIAL_CAPACITY);
    IndexedHashMap m_viniti2yandexIds = new IndexedHashMap(HASHMAP_INITIAL_CAPACITY);
    private static final int HASHMAP_INITIAL_CAPACITY = 1000;
}
