import java.io.IOException;
import java.util.Hashtable;

/**
 *  Base interface to be implemented by every custom handler for ROMIP parser.
 *  See SimplePageHandler for sample implementation.
 */

interface PageHandler
{
	void initialize(Hashtable args) throws IOException;
	void handlePage(String docID, String pageData);
	boolean startNewFile(final String strFileName) throws IOException;
}
