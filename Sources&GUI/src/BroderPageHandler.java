import java.io.IOException;
import java.util.Hashtable;

/*
 * Created on May 8, 2005
 */

/**
 * @author object
 */
public class BroderPageHandler extends BaseShinglePageHandler implements PageHandler {

	public void initialize(Hashtable args) throws IOException {
		super.initialize(args);
		final String strShinglesPerDoc = (String) args.get(SHINGLES_PER_DOC_PARAMETER);
		if(null != strShinglesPerDoc)
		{
			m_nShinglesPerDoc = Integer.parseInt(strShinglesPerDoc);
		}
	}

	public void handlePage(String docID, String pageData) {
		super.handlePage(docID, pageData);
		final String strDocNo = registerDoc(docID);

		pageData = strip(pageData);
		
		FixedSizeSortedIntList list = new FixedSizeSortedIntList(m_nShinglesPerDoc);		
		String strShingle = getNextShingle(pageData);
		while (null != strShingle) {
			list.add(getHash(strShingle));
			strShingle = getNextShingle(pageData);
		}
		
		FixedSizeSortedIntList.IntNode node = list.getFirst();
		int i = 0;
		while (null != node) {
			registerFingerprint(strDocNo, node.getInt(), i++);
			node = node.getNext();
		}
	}
	
	private int m_nShinglesPerDoc = 100;
}
