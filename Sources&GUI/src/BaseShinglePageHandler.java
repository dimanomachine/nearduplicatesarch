import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Hashtable;

/*
 * Created on May 8, 2005
 */

/**
 * @author object
 */
public class BaseShinglePageHandler implements PageHandler {

	public void initialize(Hashtable args) throws IOException {
		if (null == args) {
			throw new IllegalArgumentException();
		}
		m_strOutFolder = (String) args.get(OUT_PARAMETER);
		m_strOutIdsFolder = (String) args.get(OUT_IDS_PARAMETER);
		if (null == m_strOutFolder || null == m_strOutIdsFolder) {
			throw new IllegalArgumentException(ERR_OUT);
		}
		
		final String strLength = (String) args.get(LENGTH_PARAMETER);
		if (null != strLength) {
			m_nLength = Integer.parseInt(strLength);
		}
		
		final String strOffset = (String) args.get(OFFSET_PARAMETER);
		if (null != strOffset) {
			m_nOffset = Integer.parseInt(strOffset);
		}

		m_fStrip = Boolean.parseBoolean((String) args.get(STRIP_PARAMETER));
		
		final String strFirstIndex = (String) args.get(INDEX_PARAMETER);
		if (null != strFirstIndex) {
			int nFirstIndex = Integer.parseInt(strFirstIndex);
			if (0 <= nFirstIndex) {
				m_nNumberOfFiles = nFirstIndex;
			}
		}
	}
	
	public void handlePage(String docID, String pageData) {
		System.out.println("id=" + docID + " len=" + pageData.length());
		m_nBeginIndex = 0;
		pageData = "fff";
	}
	
	public boolean startNewFile(final String strFileName) throws IOException
	{
		boolean fNew = true;
		File fileIds = new File(m_strOutIdsFolder, strFileName + FILE_IDS_EXTENSION);
		if (fileIds.exists()) {
			fNew = false;
		} else {
			m_pwFileIds = new PrintWriter(new FileWriter(m_strOutIdsFolder + '/' + strFileName + FILE_IDS_EXTENSION), true);
			m_pwShingles = new PrintWriter(new FileWriter(m_strOutFolder + '/' + strFileName + SHINGLES_EXTENSION), true);
		}
		return fNew;
	}

	protected String registerDoc(final String strDocId)
	{
		final String strDocNo = String.valueOf(m_nNumberOfFiles++);
		m_pwFileIds.println(strDocNo + DELIMITER + strDocId);
		return strDocNo;
	}
	
	protected void registerShingle(final String strFileId, final String strShingle, final int nNumber)
	{
		registerFingerprint(strFileId, getHash(strShingle), nNumber);
	}

	protected void registerFingerprint(final String strFileId, final int nFingerprint, final int nNumber)
	{
		m_pwShingles.println(strFileId + DELIMITER + nFingerprint + DELIMITER + nNumber);
	}
	
	protected String getNextShingle(final String strData)
	{
		int nEndIndex = m_nBeginIndex + m_nLength;
		if (strData.length() < nEndIndex) {
			return null;
		}
		final String strShingle = strData.substring(m_nBeginIndex, nEndIndex);
		m_nBeginIndex += getOffset();
		return strShingle;
	}
	
	protected int getOffset()
	{
		return m_nOffset;
	}
	
	protected String strip(String str)
	{
		return m_fStrip ? HtmlStripper.strip(str) : str;
	}
	
	protected String getIdsFolder()
	{
		return m_strOutIdsFolder;
	}

	protected static int getHash(final String strShingle)
	{
		return strShingle.hashCode();
	}
	
	public static final String DELIMITER = "\t";

	private PrintWriter m_pwFileIds; 
	private PrintWriter m_pwShingles;

	private int m_nLength = 20;
	private int m_nBeginIndex = 0;
	private int m_nNumberOfFiles = 0;
	private int m_nOffset = 1;
	private String m_strOutFolder;
	private String m_strOutIdsFolder;
	private boolean m_fStrip = false;
	
	
	private static final String SHINGLES_EXTENSION = ".fps";
	private static final String FILE_IDS_EXTENSION = ".ids";

	protected static final String SHINGLES_PER_DOC_PARAMETER = "-n";
	private static final String LENGTH_PARAMETER = "-l";
	private static final String OFFSET_PARAMETER = "-o";
	private static final String STRIP_PARAMETER = "-s";
	private static final String OUT_PARAMETER = "-out";
	private static final String OUT_IDS_PARAMETER = "-ids";
	private static final String INDEX_PARAMETER = "-i";

	private static final String ERR_OUT = "-out (output folder for fingerprints) or -ids (output folder for doc ids) parameter is missing";
}
